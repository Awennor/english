﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DataHolderPy : MonoBehaviour {

    [SerializeField]
    List<NodePy> nodes = new List<NodePy>();
    Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
    Dictionary<string, CharaInfoPy> charas = new Dictionary<string, CharaInfoPy>();

    Dictionary<string, int> labelPositions = new Dictionary<string, int>();
    CalendarData calendarData = new CalendarData();

    internal ConversationData GetConversation(string v) {
        return convs[v];
    }

    Dictionary<string, ConversationData> convs = new Dictionary<string, ConversationData>();

    public List<NodePy> Nodes {
        get {
            return nodes;
        }

        set {
            nodes = value;
        }
    }

    public CalendarData CalendarData {
        get {
            return calendarData;
        }

        set {
            calendarData = value;
        }
    }

    internal void DebugLabelData() {
        foreach (KeyValuePair<string, int> entry in labelPositions) {
            Debug.Log(entry.Value + " Label - Position " + entry.Key);

        }
    }

    internal int LabelPos(string v) {
        if (labelPositions.ContainsKey(v))
            return labelPositions[v];
        else
            return -1;
    }

    internal NodePy GetNode(int v) {
        return Nodes[v];
    }

    // Use this for initialization
    void Start() {
        Debug.Log("DATA HOLDER START");
    }

    // Update is called once per frame
    void Update() {

    }

    internal void RegisterImage(string imageName, Sprite sprite) {
        sprites.Add(imageName, sprite);
    }

    internal void RegisterCharacter(string variableName, string charaName, string imageName) {
        var c = new CharaInfoPy();
        c.Name = charaName;
        c.ImageName = imageName;
        charas.Add(variableName, c);
    }

    internal NodePy RegisterSay(string speaker, string talk) {

        NodePy n = NewNode(NodeType.SAY);
        SayNodeInfo(speaker, talk, n);
        return n;
    }

    public NodePy SayNodeInfo(string speaker, string talk, NodePy n) {

        n.AddData(NodeDataEnum.SAY_SPEAKER, speaker);
        n.AddData(NodeDataEnum.SAY_TALK, talk);
        return n;
    }

    internal string CharaName(string speaker) {
        return charas[speaker].Name;
    }

    internal CharaInfoPy Chara(string speaker) {
        return charas[speaker];
    }

    internal void RegisterConversation(ConversationData conversationDataNow, string conversationId) {
        convs[conversationId] = conversationDataNow;
        NodePy n = NewNode(NodeType.CONVERSATIONASSEMBLE);
        n.AddData(NodeDataEnum.CONVERSATION_ID, conversationId);
    }

    internal Sprite GetSprite(string imageToShow) {
        Debug.Log(imageToShow + "IMAGETOSHOW");
        return sprites[imageToShow];
    }

    private NodePy NewNode(NodeType t) {
        var n = new NodePy();
        n.Type = t;
        Nodes.Add(n);
        return n;
    }

    internal void RegisterShow(string imName, string atCommand) {
        NodePy n = NewNode(NodeType.SHOW);
        n.AddData(NodeDataEnum.SHOW_IMAGE, imName);
        n.AddData(NodeDataEnum.SHOW_AT, atCommand);
        Debug.Log(imName + "IMAGE NAME");
    }

    internal void RegisterScene(string imName) {
        NodePy n = NewNode(NodeType.SCENE);
        n.AddData(NodeDataEnum.SHOW_IMAGE, imName);
        Debug.Log(imName + "IMAGE NAME");
    }

    internal void RegisterHide(string imName) {
        NodePy n = NewNode(NodeType.HIDE);
        n.AddData(NodeDataEnum.HIDE_IMAGE, imName);
        //Debug.Log(imName+"IMAGE NAME");
    }

    internal void RegisterLogSend(string v) {
        NodePy n = NewNode(NodeType.LOGSEND);
        n.AddData(NodeDataEnum.LOG_TAG, v);
    }

    internal void EndGameNode() {
        RegisterSay(null, "End of the Story, you may close the application.");
        NewNode(NodeType.ENDGAME);

        
    }

    internal void RegisterLabel(string labelname) {
        if (calendarData.DayToLabel.ContainsValue(labelname)) {
            NewNode(NodeType.CALENDARADVANCEDAY);
        }
        int labelPos = nodes.Count;
        labelPositions[labelname] = labelPos;

    }
}
