﻿using UnityEngine;
using System.Collections;
using System.Text;

public class LogMailAssembler : MonoBehaviour {

    public EmailSender emailSender;
    public MessageTextCollector messageTextCollector;
    StringBuilder stringB = new StringBuilder();
    [SerializeField]
    string experimentID ="";

    

    public string ExperimentID {
        get {
            return experimentID;
        }

    }

    public void SetExperimentID(string id) {
        experimentID = id;
    }

    public void LogSend(string tag) {
        var times = messageTextCollector.Times;
        var words = messageTextCollector.WordCount;
        var charas = messageTextCollector.CharacterCount;
        
        var m = stringB;
        m.Length = 0;
        //m.Append( tag+",,\n");
        m.Append("Reading Time, Word Count, Character count\n");
        for (int i = 0; i < times.Count; i++) {
            m.Append(times[i]);
            m.Append(',');
            m.Append(words[i]);
            m.Append(',');
            m.Append(charas[i]);
            m.Append('\n');
        }

        var subject = "LogData "+ExperimentID+" " + tag;
        //emailSender.SendEmail(subject, m.ToString());
        emailSender.SendMailWithCsv(subject, subject +".csv", m.ToString());

    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
