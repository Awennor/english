﻿using UnityEngine;
using System.Text;
#if (UNITY_EDITOR)
using UnityEditor;
#endif

public class TranslatorToDoc : MonoBehaviour {

    public DataHolderPy dataHolder;
    string lastSpeaker;
    bool firstMessage;
    private bool lastSpeakerHadNoName;
    StringBuilder stringBuilder = new StringBuilder();
    bool removeArrow = true;
    bool stopAtConversationBadEnd = false;
    bool upperCaseNames = true;

    public bool RemoveArrow {
        get {
            return removeArrow;
        }

        set {
            removeArrow = value;
        }
    }

    public bool StopAtConversationBadEnd {
        get {
            return stopAtConversationBadEnd;
        }

        set {
            stopAtConversationBadEnd = value;
        }
    }

    public bool UpperCaseNames {
        get {
            return upperCaseNames;
        }

        set {
            upperCaseNames = value;
        }
    }

    //    private bool processingFirstNode;

    public void StartTranslating() {
        if (isActiveAndEnabled) {
            stringBuilder.Length = 0;
            var nodes = dataHolder.Nodes;
            string finalString = NodeToString(nodes);

            Debug.Log(finalString);
        }
    }

    public string NodeToString(System.Collections.Generic.List<NodePy> nodes) {
        stringBuilder.Length = 0;
        firstMessage = true;
        for (int i = 0; i < nodes.Count; i++) {
            //processingFirstNode = i==0;
            var n = nodes[i];
            if (n.Type == NodeType.CONVERSATIONASSEMBLE) {
                string convId = n.GetData(NodeDataEnum.CONVERSATION_ID) as string;
                var conversation = dataHolder.GetConversation(convId);
                var talkNodes = conversation.TalkNodes;
                for (int j = 0; j < talkNodes.Count; j++) {
                    if (talkNodes[j].TalkType == ConvTalkType.FIXED ||
                        talkNodes[j].TalkType == ConvTalkType.MOVABLE) {
                        NodePy n2 = talkNodes[j].Node;

                        ProcessSayNode(stringBuilder, n2);
                    }

                }

            }
            if (n.Type == NodeType.CONVERSATIONBADEND) {
                break;
            }
            if (n.Type == NodeType.SAY) {
                ProcessSayNode(stringBuilder, n);
            }
            if (n.Type == NodeType.CONTROLGROUPTIP) {
                ProcessSayNode(stringBuilder, n);
            }
        }
        var finalString = stringBuilder.ToString();
#if (UNITY_EDITOR)
        EditorGUIUtility.systemCopyBuffer = (finalString);
#endif
        return finalString;
    }

    private void ProcessSayNode(StringBuilder stringBuilder, NodePy n) {
        var speaker = n.GetData(NodeDataEnum.SAY_SPEAKER) as string;
        bool speakerChange;

        if (lastSpeaker == null) {
            speakerChange = lastSpeaker != speaker;
        } else {
            if (speaker == null) {
                speakerChange = true;
            } else {
                speakerChange = !lastSpeaker.Equals(speaker);
            }

        }

        lastSpeaker = speaker;

        if (firstMessage) {
            if (speaker != null)
                speakerChange = true;
            firstMessage = false;
        }
        if (speakerChange) {
            //if(!processingFirstNode)
            stringBuilder.AppendLine();
            if (speaker != null) {
                var charaName = dataHolder.CharaName(speaker);

                if (charaName != null && charaName.Length > 0) {
                    if (lastSpeakerHadNoName) {
                        stringBuilder.AppendLine();
                    }
                    lastSpeakerHadNoName = false;
                    string upperName = charaName;
                    if(upperCaseNames)
                        upperName = charaName.ToUpper();
                    stringBuilder.Append(upperName);
                    stringBuilder.Append(":");

                } else {
                    lastSpeakerHadNoName = true;
                    stringBuilder.AppendLine();
                }
            } else {
                lastSpeakerHadNoName = true;
                stringBuilder.AppendLine();
            }
        }
        stringBuilder.Append(' ');
        var stringMessage = n.GetData(NodeDataEnum.SAY_TALK) as string;
        stringMessage = stringMessage.Replace('\n', ' ');

        if(removeArrow) {
            stringMessage = stringMessage.Replace("> ", "");
            stringMessage = stringMessage.Replace(">", "");
        }
            
        
        stringBuilder.Append(stringMessage);
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
