﻿using UnityEngine;
using System.Collections;

public class DayLineUI : MonoBehaviour {

    [SerializeField]
    DayObject[] dayObject;

    public DayObject[] Days {
        get {
            return dayObject;
        }

        set {
            dayObject = value;
        }
    }
}
