﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CalendarInterpreterBridge : MonoBehaviour {

    public CalendarUI calendarUI;
    int dayIndex = 0;
    public Interpreter interpreter;
    public DataHolderPy dataHolder;
    //Text text;



    public void InterpreterCallbackAdvanceDay() {
        dayIndex++;
        calendarUI.ShowCalendar();
        calendarUI.CurrentDay(dayIndex);
    }

    public void CalendarButtonPressCallback() {
        calendarUI.HideCalendar();
        //dataHolder.CalendarData.DayToLabel.Values;
        int index = 0;
        var dayToLabel = dataHolder.CalendarData.DayToLabel;
        foreach (KeyValuePair<int, string> entry in dayToLabel) {
            if(index == dayIndex) {
                interpreter.StartOnLabel(entry.Value);
                return;
            }
            index++;
        }
    }

	// Use this for initialization
	void Start () {
	    calendarUI.ShowCalendar();
        calendarUI.CurrentDay(dayIndex);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
