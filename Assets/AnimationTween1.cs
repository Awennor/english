﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class AnimationTween1 : MonoBehaviour {

    Image image;

	// Use this for initialization
	void Start () {
	    image = GetComponent<Image>();
	}

    public void Animate() {
        image.DOKill();
        DOTween.Sequence()
            .Append(image.DOFade(0,0.1f))
            .Append(image.DOFade(1,0.1f))
            .Append(image.DOFade(0,0.1f))
            .Append(image.DOFade(1,0.1f))
            .Append(image.DOFade(0.4f,0.1f))
            ;

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
