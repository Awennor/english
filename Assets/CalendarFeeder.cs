﻿using UnityEngine;
using System.Collections;
using System;

public class CalendarFeeder : MonoBehaviour {

    public DataHolderPy dataHolderPy;
    public TextAsset calendarScript;
    

    // Use this for initialization
    void Awake () {
	    string text = calendarScript.text;
        var lines = text.Split('\n');
        for (int i = 0; i < lines.Length; i++) {
            string line = lines[i];
            var words = line.Split(' ');
            var dayNumber = Convert.ToInt32(words[1]); 
            string label = words[3];
            dataHolderPy.CalendarData.AddLabel(dayNumber, label);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
