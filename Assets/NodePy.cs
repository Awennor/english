﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class NodePy {

    [SerializeField]
    NodeType type;

    [SerializeField]
    Dictionary<int, object> nodeData = new Dictionary<int, object>();


    public NodeType Type {
        get {
            return type;
        }

        set {
            type = value;
        }
    }

    internal void AddData(NodeDataEnum dataType, object data) {
        nodeData[(int)dataType] = data;
    }

    internal object GetData(NodeDataEnum dataType) {
        return nodeData[(int)dataType];
    }
}

public enum NodeType {
    SAY, SHOW, SCENE,
    CONVERSATIONASSEMBLE,
    CONVERSATIONBADEND,
    HIDE,
    CONVERSATIONCLUEINSTRUCTION,
    LOGSEND,
    CALENDARADVANCEDAY,
    ENDGAME,
    CONTROLGROUPTIP,
}

public enum NodeDataEnum {
    SAY_TALK, SAY_SPEAKER,
    SHOW_IMAGE,
    CONVERSATION_ID,
    SHOW_AT,
    HIDE_IMAGE,
    LOG_TAG
}
