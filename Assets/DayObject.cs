﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DayObject : MonoBehaviour {

    [SerializeField]
    private Text text;
    [SerializeField]
    private Image image;
    [SerializeField]
    private Button button;

    public Image Image {
        get {
            return image;
        }

        set {
            image = value;
        }
    }

    public Text Text {
        get {
            return text;
        }

        set {
            text = value;
        }
    }

    public Button Button {
        get {
            return button;
        }

        set {
            button = value;
        }
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
