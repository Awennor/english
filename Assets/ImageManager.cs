﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using DG.Tweening;

public class ImageManager : MonoBehaviour {

    public GameObject parent;
    public DataHolderPy dataHolder;
    public Dictionary<string, Image> imageObjectsTagged = new Dictionary<string, Image>();

    // Use this for initialization
    void Start() {
        /*
	    var background = Resources.Load<Sprite>("background");
        var gameObject = new GameObject();
        var image = gameObject.AddComponent<Image>();
        image.sprite= background;
        Debug.Log(background);
        gameObject.transform.SetParent(parent.transform);
        gameObject.GetComponent<RectTransform>().sizeDelta 
            = new Vector2(background.rect.width, background.rect.height);
        gameObject.transform.localScale = new Vector3(1,1,1);
        gameObject.transform.position = Vector3.zero;
        gameObject.transform.localPosition= Vector3.zero;
        */
    }

    internal void ShowImage(string imageToShow, float x, float y, bool fade = true) {
        //Debug.Log(imageToShow+"SHOWING...");

        string tag = imageToShow.Split(' ')[0];
        Debug.Log(imageToShow + "2_2");
        Debug.Log(tag + "@_@");
        var background = dataHolder.GetSprite(imageToShow);
        Image image = null;
        if (imageObjectsTagged.ContainsKey(tag)) {
            image = imageObjectsTagged[tag];
        } else {
            var gameObject = new GameObject();
            image = gameObject.AddComponent<Image>();
            imageObjectsTagged[tag] = image;

        }

        image.sprite = background;
        image.transform.SetParent(parent.transform);
        image.GetComponent<RectTransform>().sizeDelta
            = new Vector2(background.rect.width, background.rect.height);
        image.transform.localScale = new Vector3(1, 1, 1);
        if(fade) {
            image.color = new Color(1, 1, 1, 0);
            image.DOFade(1, 0.5f);
        } else {
            image.color = new Color(1, 1, 1, 1);
        }
        

        Vector2 sizeRef = image.canvas.GetComponent<CanvasScaler>().referenceResolution;

        //gameObject.transform.position = Vector3.zero;
        x -= 0.5f;
        y -= 0.5f;

        image.transform.localPosition = new Vector3(x
            * sizeRef.x
            //Screen.width
            //-background.rect.width/2
            ,
            y
            //*Screen.height
            * sizeRef.y
            //-background.rect.height/2
            );
        //gameObject.transform.localPosition= new Vector3(0,0);
    }

    internal Image GetActiveImage(string imageName) {
        if (imageObjectsTagged.ContainsKey(imageName)) {
            return imageObjectsTagged[imageName];
        }
        return null;
    }

    // Update is called once per frame
    void Update() {

    }

    internal void HideAll(bool gradual) {
        var vs = imageObjectsTagged.Values;
        var numerator = vs.GetEnumerator();
        while (numerator.MoveNext()) {
            var current = numerator.Current;
            if(gradual)
                current.DOFade(0,0.5f);
            else
                current.color = new Color(0,0,0,0);
            //current.gameObject.SetActive(false);

        }
    }

    internal void HideImage(string imageToShow, bool gradual = true) {
        if (imageObjectsTagged.ContainsKey(imageToShow)) {
            var im = imageObjectsTagged[imageToShow];
            if(gradual) {
                im.DOFade(0, 0.5f);
            } else {
                im.color = new Color(0,0,0,0);
            }
            

        }

            //.gameObject.SetActive(false);
    }
}
