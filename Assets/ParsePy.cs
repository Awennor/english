﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;
using UnityEngine.Events;

public class ParsePy : MonoBehaviour {

    public GameObject scenePy;

    public TextAsset[] pyScripts;

    public DataHolderPy dataHolderPy;
    
    List<string> lines = new List<string>();
    List<string> aux = new List<string>();
    public UnityEvent finishedParsing = new UnityEvent();

    public ConversationDataParseAux conversationDataParse;
    public bool ParsingConversation { get; private set; }
    int conversationTab;
    //List<int> blockTabs = new List<int>();

    // Use this for initialization
    void Start () {
        Debug.Log("BEFORE LOOP");
        for (int i = 0; i < pyScripts.Length; i++) {
            string text = pyScripts[i].text;
            text = text.Replace("\r\n", "\n");
            //remove comments
            text = new Regex("(#.*\n)").Replace(text, "");
            //remove blank lines
            text = Regex.Replace(text, @"^\s*$[\r\n]*", "", RegexOptions.Multiline);
            var logicalLines = text.Split('\n');
            lines.AddRange(logicalLines);
            //this code does not protect against multiline parenthesis
        }

        for (int i = 0; i < lines.Count; i++) {
            //Debug.Log("Each line...");
            //splits a line in to words

            var phrase = lines[i];
            int tabCount = 0;
            for (int j = 0; j < phrase.Length; j++) {
                if(phrase[j] == '\t') {
                    tabCount++;
                } else {
                    break;
                }
            }
            
            if(ParsingConversation && tabCount<= conversationTab) {
                ParsingConversation = false;
                conversationTab = -1;
                Debug.Log("CONVERSATION END");
            }
            phrase = phrase.Replace("\t", "");
            //Debug.Log("PHRASE___"+phrase);
            var wordsArray = phrase.Split(' ');
            NotEmptyWords(wordsArray, aux);
            var words = aux;
            

            
            /*
            for (int j = 0; j < words.Count; j++) {
                Debug.Log(words[j] + " w "+j);
            }
            //*/
            int firstWordIndex = 0;
            var firstWord = words[firstWordIndex];
            bool firstWordIsString = firstWord.Contains("\"");
            int numberWords = words.Count;
            
            bool processAsFakeSayNode = false;
            NodeType fakeSayNodeType = NodeType.SAY;
            

            if(firstWord.Equals("CreateConversation:")) {
                conversationDataParse.CreateConversation();
                
                ParsingConversation = true;
                conversationTab = tabCount;
                Debug.Log("CONVERSATION START");
                continue;
            }
            if(conversationDataParse && conversationDataParse.ConversationCommand(phrase, words)) {
                continue;
            }

            if(firstWord.Equals("define")) {
                ProcessDefine(words);
                continue;
            }
            if(firstWord.Equals("logsend")) {
                dataHolderPy.RegisterLogSend(words[1]);
                continue;
            }
            if(firstWord.Equals("label")) {
                ProcessLabel(words);
                continue;
            }
            if(firstWord.Equals("show")) {
                ProcessShow(words);
                continue;
            }
            if(firstWord.Equals("hide")) {
                ProcessHide(words);
                continue;
            }
            if(firstWord.Equals("scene")) {
                ProcessScene(words);
                continue;
            }
            if(firstWord.Equals("endgame")) {
                dataHolderPy.EndGameNode();
                continue;
            }
            if (firstWord.Equals("image")) {
                ProcessImage(words);
                continue;
            }
            if(firstWord.Equals("controlgroupsay")) {
                //TRANSFORM THIS IN TO A FAKE SAY NODE
                //-----------------------------------------------------------------
                //make sure words are in say node format
                for (int j = 1; j < words.Count; j++) {
                    words[j-1] = words[j];
                }
                firstWord = words[firstWordIndex];
                firstWordIsString = firstWord.Contains("\"");
                //-----------------------------------------------------------------
                
                processAsFakeSayNode = true;
                fakeSayNodeType = NodeType.CONTROLGROUPTIP;
            }
            
            if (firstWordIsString 
                || (numberWords > firstWordIndex+1 && words[firstWordIndex + 1].Contains("\""))
                || processAsFakeSayNode) {
                string talk = null;
                string speaker = null;
                if(firstWordIsString) {
                    talk  = firstWord;
                } else {
                    speaker = firstWord;
                    talk = words[firstWordIndex + 1];
                }
                talk = phrase.Split('"')[1];
                talk = talk.Replace("\\n", "\n");
                talk = talk.Replace("{b}", "<b>");
                talk = talk.Replace("{/b}", "</b>");
                //talk = talk.Replace("\"", "");
                //Debug.Log(speaker + " --- "+talk);
                if(ParsingConversation) {
                    conversationDataParse.RegisterSay(speaker, talk);
                    //dataHolderPy.RegisterSay(speaker, talk);
                } else {
                    NodePy n = dataHolderPy.RegisterSay(speaker, talk);
                    n.Type = fakeSayNodeType;
                }
                
                
            }

        }
        
        Debug.Log("AFTER LOOP");
        finishedParsing.Invoke();
        //return;
	    
        //gameObject.transform.position = new Vector3(background.rect.width/4,background.rect.height/4);;
	}

    private void ProcessLabel(List<string> words) {
        string labelname = words[1];

        labelname = labelname.Replace(":", "");
        dataHolderPy.RegisterLabel(labelname);
    }

    private void ProcessDefine(List<string> words) {
        string variableName = words[1];
        if (words[3].Contains("(")) {
            var typeAndArgument = words[3].Split('(');
            var typeName = typeAndArgument[0];
            if (typeName.Equals("Character")) {
                string imageName = null;
                var argument1 = typeAndArgument[1];
                string charaName = argument1.Replace("\",", "");
                charaName = charaName.Replace("\"", "");
                var commandEndsHere = argument1.Contains(")");
                if (commandEndsHere) {
                    charaName = charaName.Replace(")", "");
                } else {
                    string nextArgument = words[4];
                    var nextArgumentWords = nextArgument.Split('=');
                    if(nextArgumentWords[0].Equals("image")) {
                        imageName = nextArgumentWords[1].Replace(")", "");
                        imageName = imageName.Replace("\"", "");
                    }

                    if(!nextArgument.Contains(")")) {
                        //get next argument... organize this in a loop man, someday
                    }
                }
                dataHolderPy.RegisterCharacter(variableName, charaName, imageName);

            }

            //words[3].Split
        }
    }

    private void ProcessShow(List<string> words) {
        int j = 1;
        var imName = GetImageName(words, ref j);
        string AtCommand = GetAtCommand(words);
        
        dataHolderPy.RegisterShow(imName, AtCommand);
    }

    private void ProcessHide(List<string> words) {
        var imName = words[1];
        
        dataHolderPy.RegisterHide(imName);
    }


    private void ProcessScene(List<string> words) {
        int j = 1;
        string imName = null;
        if(words.Count > 1)
            imName = GetImageName(words, ref j);
        dataHolderPy.RegisterScene(imName);
    }

    private string GetAtCommand(List<string> words) {
        for (int j = 1; j < words.Count; j++) {
            if (words[j].Equals("at")) {
                return words[j+1];
            }
        }
        return null;
    }

    private void ProcessImage(List<string> words) {
        
        int j = 1;
        var imageName = GetImageName(words, ref j);

        var fileRaw = words[j + 1];

        string file = fileRaw.Replace("\"", "");
        dataHolderPy.RegisterImage(imageName, Resources.Load<Sprite>(file));

        //Debug.Log(imageName+"IMAGE");
        //Debug.Log(file"+file");
    }

    private static string GetImageName(List<string> words, ref int j) {
        string imageName = "";
        for (; j < words.Count; j++) {
            if (!words[j].Equals("=") && !words[j].Equals("at")) {
                if (j > 1) {
                    imageName += ' ';
                }
                imageName += words[j];
                
            } else {
                break;
            }
        }
        return imageName;
    }

    private void NotEmptyWords(string[] v, List<string> aux) {
        aux.Clear();
        for (int i = 0; i < v.Length; i++) {
            if(!v[i].Equals("")) {
                aux.Add(v[i]);
            }
        }
    }

    // Update is called once per frame
    void Update () {
	
	}
}
