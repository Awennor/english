﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class IntVectorCustom {

    [SerializeField]
    List<int> values = new List<int>();
    public int DefaultValue { get; private set; }
    public int Length {
        get {
            return values.Count;
        }

        set {
            ForceLength(value);
        }
    }

    public int Count {
        get {
            return values.Count;
        }

        set {
            ForceLength(value);
        }
    }


    public IntVectorCustom(int defaultEmptyValue = -1) {
        DefaultValue = defaultEmptyValue;
    }

    internal void Clear() {
        values.Clear();
    }

    public int this[int index] {
        get {
            FixLength(index + 1);
            return values[index];
        }

        set {
            FixLength(index + 1);
            values[index] = value;
        }
    }

    public void FixLength(int minLength) {
        while (values.Count < minLength) {
            values.Add(DefaultValue);
        }
    }

    public void ForceLength(int length) {
        FixLength(length);
        while (values.Count > length) {
            values.RemoveAt(values.Count - 1);
        }
    }

}
