﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Events;
using UnityEngine.UI;
using DG.Tweening;

public class CalendarUI : MonoBehaviour {

    [SerializeField]
    DayLineUI[] dayLines;

    public DataHolderPy dataHolder;
    public UnityEvent ClickedOnDay;

    public GameObject characterIcon;

    internal void HideCalendar() {
        gameObject.SetActive(false);
    }

    internal void ShowCalendar() {
        gameObject.SetActive(true);
    }

    internal void CurrentDay(int dayIndex) {

        //Debug.Log("Day Index "+dayIndex);
        var calendar = dataHolder.CalendarData;
        var dayToLabel = calendar.DayToLabel;
        int dayWalker = 0;

        for (int i = 0; i < dayLines.Length; i++) {
            var days = dayLines[i].Days;
            for (int j = 0; j < days.Length; j++) {
                
                days[j].Button.interactable = false;
            }
        }

        Canvas.ForceUpdateCanvases();
        foreach (KeyValuePair<int, string> entry in dayToLabel) {
            if (dayWalker == dayIndex) {

                int key = entry.Key;
                DayObject day = GetDay(key);
                var dayPos = day.transform.position;
                characterIcon.transform.DOMove(dayPos, 0.5f).SetDelay(0.5f);
                day.Button.interactable = true;
                //day.transform.SetAsLastSibling();
                break;
            }
            dayWalker++;
        }
    }

    // Use this for initialization
    void Start() {
        Debug.Log("AWAKE CALENDAR UI");
        int dayNumber = 1;
        int maxDay = 31;

        for (int i = 0; i < dayLines.Length; i++) {
            var days = dayLines[i].Days;
            for (int j = 0; j < days.Length; j++) {
                var day = days[j];
                var underMax = dayNumber <= maxDay;
                day.Text.text = "" + dayNumber++;

                day.Text.gameObject.SetActive(underMax);
                day.Image.gameObject.SetActive(false);
                day.Button.onClick.AddListener(ButtonPress);
            }
        }
        var calendar = dataHolder.CalendarData;
        var dayToLabel = calendar.DayToLabel;
        var dayNumbers = dayToLabel.Keys;

        foreach (KeyValuePair<int, string> entry in dayToLabel) {
            var label = entry.Value;
            int key = entry.Key;
            DayObject day = GetDay(key);
            day.Image.gameObject.SetActive(true);
        }
    }

    private void ButtonPress() {
        //gameObject.SetActive(false);
        ClickedOnDay.Invoke();

    }

    private DayObject GetDay(int dayIndex) {
        int dayNumber = 1;


        for (int i = 0; i < dayLines.Length; i++) {
            var days = dayLines[i].Days;
            for (int j = 0; j < days.Length; j++) {
                var day = days[j];
                if (dayNumber == dayIndex) {
                    return day;
                }
                dayNumber++;
            }
        }
        return null;
    }

    // Update is called once per frame
    void Update() {

    }
}
