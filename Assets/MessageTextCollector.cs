﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MessageTextCollector : MonoBehaviour {

    private float time;
    [SerializeField]
    List<float> times = new List<float>();
    [SerializeField]
    List<int> wordCount = new List<int>();
    [SerializeField]
    List<int> characterCount = new List<int>();
    private string latestMessage;

    public MyStringFloatEvent MessageProcessed;

    public List<float> Times {
        get {
            return times;
        }

        set {
            times = value;
        }
    }

    public List<int> WordCount {
        get {
            return wordCount;
        }

        set {
            wordCount = value;
        }
    }

    public List<int> CharacterCount {
        get {
            return characterCount;
        }

        set {
            characterCount = value;
        }
    }

    public void MessageStart(string message) {
        latestMessage = message;
        int length = message.Length;
        int words = message.Split(' ').Length;
        WordCount.Add(words);
        CharacterCount.Add(length);
        time = Time.time;
    }

    public void MessageEnd() {
        float newTime = Time.time;
        float delta = newTime - time;
        Times.Add(delta);
        MessageProcessed.Invoke(latestMessage, delta);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
