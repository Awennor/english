﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class CharaInfoPy {
    string name;
    string imageName;

    public string Name {
        get {
            return name;
        }

        set {
            name = value;
        }
    }

    public string ImageName {
        get {
            return imageName;
        }

        set {
            imageName = value;
        }
    }
}
