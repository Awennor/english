﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;
using System;

public class ConversationLogCollector : MonoBehaviour {

    StringBuilder stringBuilder = new StringBuilder();
    public LogMailAssembler messageTextCollector;
    float thinkingTime;
    private bool countingThinking;
    public string debugString;
    int numberOfTries;
    private bool readingFeedback;
    public EmailSender emailSender;
    private string latestConvId;

    public void ConversationStart(string id) {
        latestConvId = id;
        stringBuilder.Length = 0;
        stringBuilder.Append("Conversation Log");
        stringBuilder.AppendLine();
        stringBuilder.Append("User ID "+messageTextCollector.ExperimentID);
        stringBuilder.AppendLine();
        stringBuilder.Append("Conv ID "+id);
        stringBuilder.AppendLine();
        debugString = stringBuilder.ToString();
        numberOfTries = 0;
        readingFeedback = false;
    }

    public void BuildStart() {
        numberOfTries++;
        countingThinking = true;
        thinkingTime = 0;
        stringBuilder.AppendLine("Build Start");
        debugString = stringBuilder.ToString();
        readingFeedback = false;
        //SendMail();
    }

    public void CardSlotChange(int slot, string textOfCard) {
        stringBuilder.AppendFormat("Thinking {0}s", thinkingTime.ToString("0.00"));
        stringBuilder.AppendLine();
        stringBuilder.AppendFormat("Card in Slot {0} \"{1}\"",slot,textOfCard);
        stringBuilder.AppendLine();
        thinkingTime = 0;
        debugString = stringBuilder.ToString();
    }

    public void MessageProcessed(string message, float timeReading) {
        if(readingFeedback) {
            stringBuilder.AppendFormat("\"{0}\" {1}s", message, timeReading.ToString("0.00"));
            stringBuilder.AppendLine();
            debugString = stringBuilder.ToString();
            //stringBuilder.AppendLine(message+ (int)timeReading+"s");
        }
    }

    public void AssembleComplete(bool success) {
        countingThinking = false;
        thinkingTime = 0;
        if(success) {
            stringBuilder.AppendLine("Answer Found after " + numberOfTries + " tries");
            SendMail();
            readingFeedback = false;
        } else {
            stringBuilder.AppendLine("Feedback Start");
            
            readingFeedback = true;
        }
        debugString = stringBuilder.ToString();
        
    }

    private void SendMail() {
        
        string subject = "Conversation Log ID " + latestConvId + " User ID " + messageTextCollector.ExperimentID;
        var fileName = subject + ".txt";
        var fileBody = stringBuilder.ToString();
        emailSender.SendMailWithTextPlain(subject, fileName, fileBody);
        try {
            string path = @"C:\Users\Pedro\Documents\English\external\logs\"+fileName;
            File.AppendAllText(path, fileBody);
        } catch (Exception ex) {
            Debug.Log("Fail!!! "+ex.Message);
        }
        
        //emailSender.SendMailWithCsv("1", "2.csv", "3");
        //emailSender.SendEmail(subject, stringBuilder.ToString());
    }

    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        if(countingThinking)
	        thinkingTime += Time.deltaTime;
	}
}

