﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using System;
using UnityEngine.Events;

public class ConversationManager : MonoBehaviour {

    public DataHolderPy dataholder;
    public GameObject graphicsHolder;
    public ConversationUI conversationUI;
    private ConversationData currentConversation;
    public Interpreter interpreter;
    bool conversationInterpreting = false;
    int talkIndex;
    List<NodePy> assembledNodes = new List<NodePy>();
    List<int> CorrectlyGuessedSlots = new List<int>();
    public Image overlay;
    public FlashTest flashText;
    public Color winTextColor;
    public Color loseTextColor;
    bool correctGuessMarking = false;
    

    public TranslatorToDoc translator;

    public Text wrongConversationText;
    public Button previousConversationButton;
    bool previousConversationVisualizationOn = false;

    public UnityEvent ShowClue;
    public UnityEvent BuildStart;
    public MyStringEvent ConversationStart;
    public MyBoolEvent assembleComplete;
    
    public MyIntStringEvent SpotOfCardChange;


    public void ProcessConversation(NodePy nodePy) {
        
        Debug.Log("CONVERSATION START PROCESS!");
        var conversationId = (string)nodePy.GetData(NodeDataEnum.CONVERSATION_ID);
        currentConversation = dataholder.GetConversation(conversationId);
        CorrectlyGuessedSlots.Clear();
        previousConversationButton.gameObject.SetActive(false);
        ConversationStart.Invoke(conversationId);

        DOTween.Sequence()
            .Append(overlay.DOFade(1f, 0.6f))
            .AppendInterval(0.5f)
            .AppendCallback(StarProcessConversation)
            .Append(overlay.DOFade(0f, 2f));
        //StarProcessConversation();
    }

    private void StarProcessConversation() {
        BuildStart.Invoke();
        Debug.Log("CONVERSATION PROCESS!");
        conversationInterpreting = true;
        interpreter.Advancing = false;
        conversationUI.StartConversationDataProcessing();
        //graphicsHolder.SetActive(true);
        graphicsHolder.transform.localPosition = new Vector3(0, 0, 0);

        var cD = currentConversation;
        var talkNodes = cD.TalkNodes;
        int slotNumber = 0;
        conversationUI.NumberOfTalkNodes(talkNodes.Count);
        for (int i = 0; i < talkNodes.Count; i++) {
            var conversationTalkNode = talkNodes[i];
            conversationUI.SpeakText(i, (string)conversationTalkNode.Node.GetData(NodeDataEnum.SAY_TALK));

            var talkType = conversationTalkNode.TalkType;
            if (talkType != ConvTalkType.DUMMY) {
                slotNumber++;
            }

        }
        conversationUI.NumberOfSlots(slotNumber);

        for (int i = 0; i < talkNodes.Count; i++) {
            var conversationTalkNode = talkNodes[i];
            var talkType = conversationTalkNode.TalkType;
            var correctlyGuessedSlot = CorrectlyGuessedSlots.Contains(i);
            conversationUI.FixNodeOn(i, talkType == ConvTalkType.FIXED, correctlyGuessedSlot);
            if(correctlyGuessedSlot) {
                conversationUI.CorrectGuessColor(i);
            }
        }
        conversationUI.EndConversationDataProcessing();
    }

    public void AssembleComplete() {
        assembledNodes.Clear();
        conversationUI.ShowDragContent();
        var talkNodes = currentConversation.TalkNodes;
        var responses = currentConversation.ConvResponses;
        int slot = 0;
        bool conversationBecameMistake = false;
        for (int i = 0; i < talkNodes.Count; i++) {
            var tN = talkNodes[i];
            if (tN.TalkType == ConvTalkType.FIXED) {
                assembledNodes.Add(tN.Node);
            } else {
                if (tN.TalkType == ConvTalkType.MOVABLE) {
                    var nodeIdOnSlot = conversationUI.NodeOnSlot(slot);
                    if(CorrectlyGuessedSlots.Contains(i)) {
                        nodeIdOnSlot = i;
                    }
                    conversationUI.DebugDragAndDropSlots();
                    Debug.Log(i+" index ");

                    var nodeChosen = talkNodes[nodeIdOnSlot];

                    var node = nodeChosen.Node;
                    Debug.Log(node.GetData(NodeDataEnum.SAY_TALK) + " TALK INSIDE " + nodeIdOnSlot);
                    assembledNodes.Add(node);
                    var wrongChoice = nodeChosen != tN;
                    if (wrongChoice) { //wrong choice case
                        if(!conversationBecameMistake) { //the conversation is deviating from here
                            var n = new NodePy();
                            n.Type = NodeType.CONVERSATIONCLUEINSTRUCTION;
                            //Debug.Log(assembledNodes.Count + "COUNT WHEN INSERTING CLUE!" +node.GetData(NodeDataEnum.SAY_TALK));
                            assembledNodes.Insert(assembledNodes.Count-1, n); //show it before sakura says weird
                        }
                        conversationBecameMistake = true;
                    }
                    for (int j = 0; j < responses.Count; j++) {

                        var r = responses[j];
                        if (r.Target == nodeChosen) {
                            //Debug.Log("response found tag type!" + r.Nodes.Count);

                            assembledNodes.AddRange(r.Nodes);
                        } else {
                            //Debug.Log("response wrong!");
                        }
                    }
                    if (wrongChoice) { //wrong choice case
                        Debug.Log("WRONG ANSWER");
                        for (int j = 0; j < responses.Count; j++) {
                            Debug.Log("responses");
                            var r = responses[j];
                            if (r.Slot == slot) {
                                Debug.Log("response found!" + r.Nodes.Count);

                                assembledNodes.AddRange(r.Nodes);
                            } else {
                                Debug.Log("response wrong!");
                            }
                        }
                    } else {
                        if(!conversationBecameMistake && correctGuessMarking) {
                            CorrectlyGuessedSlots.Add(i);
                        }
                    }


                    slot++;
                } else {
                    break;
                }

            }
        }
        assembleComplete.Invoke(!conversationBecameMistake);
        talkIndex = 0;
        conversationInterpreting = true;
        interpreter.Advancing = false;
        conversationUI.HideButton();

        DOTween.Sequence()
            .Append(overlay.DOFade(1f, 0.5f))
            .AppendInterval(0.3f)
            .AppendCallback(AssembleWindowLeft)
            .Append(overlay.DOFade(0f, 2f));

    }

    public void CardChangePosition(int draggable, int spot) {
        var talkNodes = currentConversation.TalkNodes;
        var nodeChosen = talkNodes[draggable];
        string text = nodeChosen.Node.GetData(NodeDataEnum.SAY_TALK) as string;
        SpotOfCardChange.Invoke(spot, text);

    }

    private void AssembleWindowLeft() {
        graphicsHolder.transform.position = new Vector3(0, -1000);
        Execute();
    }

    private void Execute() {
        var tN = assembledNodes[talkIndex];
        if (tN.Type == NodeType.CONVERSATIONCLUEINSTRUCTION) {
            ShowClue.Invoke();
            return;
        }
        if (tN.Type == NodeType.CONVERSATIONBADEND) {
            conversationInterpreting = false;
            flashText.Color = loseTextColor;
            translator.RemoveArrow = false;
            translator.StopAtConversationBadEnd = true;
            string s = translator.NodeToString(assembledNodes);
            wrongConversationText.text = s;
            if(previousConversationVisualizationOn)
                previousConversationButton.gameObject.SetActive(true);
            flashText.ShowMessage(
                "Sakura is disappointed with this conversation.\n\n Try again!",
                0.1f, 0.1f, 1.7f, 1f
                );
            DOTween.Sequence()
                .Append(overlay.DOFade(1f, 0.8f))
                .AppendInterval(0.8f)
                .AppendCallback(StarProcessConversation)
                .Append(overlay.DOFade(0f, 2f));
            Debug.Log("CONVERSATION REPEAT!!");
            return;
        }
        interpreter.ExecuteNode(tN);
    }

    public void InterpreterAdvance() {
        if (conversationInterpreting) {
            talkIndex++;
            if (assembledNodes.Count > talkIndex) {
                Execute();
            } else {
                conversationInterpreting = false;
                flashText.Color = winTextColor;
                flashText.ShowMessage(
                    "Sakura is happy with this conversation.\n\n Good job!",
                    0.1f, 0.1f, 1.7f, 1f
                    );
                DOTween.Sequence()
                    .Append(overlay.DOFade(1f, 0.8f))
                    .AppendInterval(0.8f)
                    .AppendCallback(GoBackToNormalFlow)
                    .Append(overlay.DOFade(0f, 2f))
                    
                ;

            }

        }
    }

    private void GoBackToNormalFlow() {
        interpreter.Advancing = true;
        
        interpreter.Advance();
    }


    // Use this for initialization
    void Start() {
        interpreter.AdvanceEvent.AddListener(InterpreterAdvance);
    }

    // Update is called once per frame
    void Update() {

    }
}
