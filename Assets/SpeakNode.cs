﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class SpeakNode : MonoBehaviour {
    [SerializeField]
    Text textUI;
    [SerializeField]
    Draggable draggable;
    public Image background;
    public string Text { get {
            return textUI.text;
        } internal set {
            textUI.text = value;
        } }

    public Draggable Draggable {
        get {
            return draggable;
        }

        set {
            draggable = value;
        }
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    

    internal void Undraggable(bool fix) {
        Draggable.enabled = !fix;
    }

    internal void BackgroundColor(Color fixedColor) {
        background.color = fixedColor;  
    }
}
