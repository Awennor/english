﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class FlashTest : MonoBehaviour {
    private Text text;

    public Color Color { set {
            text.color = value;
        } }

    // Use this for initialization
    void Start () {
	    text = GetComponent<Text>();
	}

    public void ShowMessage(string m, float delay, float fadeIn, float keep, float fadeOut) {
        gameObject.SetActive(true);
        Color c = text.color;
        c.a = 0;
        text.color = c;
        text.text = m;
        DOTween.Sequence()
            .AppendInterval(delay)
            .Append(text.DOFade(1, fadeIn))
            .AppendInterval(keep)
            .Append(text.DOFade(0, fadeOut))
            .AppendCallback(MessageGone)
            ;
    }

    private void MessageGone() {
        text.text = "";
        gameObject.SetActive(false);
    }


    // Update is called once per frame
    void Update () {
	
	}
}
