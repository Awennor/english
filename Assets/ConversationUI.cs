﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class ConversationUI : MonoBehaviour {

    public GameObject[] slots;
    public SpeakNode[] speakNodes;
    public DragAndDropManager dragAndDrop;
    public Color fixedColor;
    public Color unfixedColor;
    public Color correctColor;
    List<SpeakNode> movableNodes = new List<SpeakNode>();
    public Button button;

    internal void StartConversationDataProcessing() {
        vLg.enabled = true;
        movableNodes.Clear();
        dragAndDrop.SpotOfDraggable.Clear();
    }

    public VerticalLayoutGroup vLg;
    public GameObject speakNodesNormalHolder;
    private int slotNumber;



    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void HideButton() {
        button.gameObject.SetActive(false);
    }

    internal void SpeakText(int v1, string v2) {
        speakNodes[v1].Text = v2;
    }

    internal void FixNodeOn(int i, bool fix, bool correctGuess)
    {
        speakNodes[i].Undraggable(fix || correctGuess);
        
        if(fix) {
            //dragAndDrop.RemoveDraggable(speakNodes[i].Draggable);
            speakNodes[i].BackgroundColor(fixedColor);
        } else {
            //dragAndDrop.AddDraggable(speakNodes[i].Draggable);
            speakNodes[i].BackgroundColor(unfixedColor);
        }
        if(slots.Length > i) {
            if(fix || i >= slotNumber)
                dragAndDrop.RemoveSlot(slots[i].GetComponent<RectTransform>());
            else
                dragAndDrop.AddSlot(slots[i].GetComponent<RectTransform>());
        }
        if(fix || correctGuess) {
            
            speakNodes[i].transform.position = slots[i].transform.position;
            speakNodes[i].transform.SetParent(speakNodesNormalHolder.transform);
        } else {
            movableNodes.Add(speakNodes[i]);
        }
    }

    internal void Hide() {
        
    }

    internal void NumberOfTalkNodes(int count) {
        for (int i = 0; i < speakNodes.Length; i++) {
            speakNodes[i].gameObject.SetActive(i < count);
        }
    }

    internal void ShowDragContent() {
        var dragSpots = dragAndDrop.SpotOfDraggable;
        for (int i = 0; i < dragSpots.Count; i++) {
            Debug.Log("DRAG "+i +" ON "+dragSpots[i]);
        }
    }

    internal void EndConversationDataProcessing() {
        
        movableNodes.Shuffle();
        for (int i = 0; i < movableNodes.Count; i++) {
            movableNodes[i].transform.SetParent(vLg.transform);
        }
        //speakNodes[i].transform.SetParent(vLg.transform);
        Canvas.ForceUpdateCanvases();
        vLg.enabled = false;
    }

    internal void CorrectGuessColor(int i) {
        speakNodes[i].BackgroundColor(correctColor);
    }

    internal int NodeOnSlot(int slot) {
        return dragAndDrop.FrameIndexOnSpot(slot);
    }

    internal void NumberOfSlots(int slotNumber)
    {
        this.slotNumber = slotNumber;
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].gameObject.SetActive(i < slotNumber);
        }
        Canvas.ForceUpdateCanvases();
    }

    internal void DebugDragAndDropSlots() {
        dragAndDrop.DebugSlotPositions();
    }
}
