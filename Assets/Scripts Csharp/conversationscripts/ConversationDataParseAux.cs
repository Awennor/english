﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ConversationDataParseAux : MonoBehaviour {
    [SerializeField]
    private ConversationData conversationDataNow;
    string conversationId;
    [SerializeField]
    ConversationTalkNode conversationTalkNode;
    public DataHolderPy dataHolderPy;

    bool parsingResponse;
    private ConversationResponse conversationResponseNow;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void RegisterSay(string speaker, string talk) {
        NodePy sayNode = dataHolderPy.SayNodeInfo(speaker, talk, new NodePy());
        

        if(parsingResponse) {
            Debug.Log("SAY TO RESPONSE NODE!!");
            conversationResponseNow.Nodes.Add(sayNode);
        } else {
            Debug.Log("SAY TO CONV NODE!!");
            conversationTalkNode = new ConversationTalkNode();
            conversationTalkNode.Node = sayNode;
            conversationDataNow.TalkNodes.Add(conversationTalkNode);
        }
        
    }


    internal bool ConversationCommand(string phrase, List<string> words) {
        var firstWord = words[0];
        if(firstWord.Equals("convid")) {
            conversationId = words[1];
            dataHolderPy.RegisterConversation(conversationDataNow, conversationId);
            Debug.Log("CONVERSATION REGUSTER!");
            
            return true;
        }
        if(firstWord.Equals("saynode")) {
            string sayType = words[1];
            string sayTag = null;
            if(words.Count >= 4) {
                sayTag = words[3];
            }
            conversationTalkNode.Tag = sayTag;
            ConvTalkType type = (ConvTalkType) Enum.Parse(typeof(ConvTalkType), sayType);
            conversationTalkNode.TalkType = type;
            return true;
        }
        if(firstWord.Equals("conversationend")) {
            NodePy nodePy = new NodePy();
            nodePy.Type = NodeType.CONVERSATIONBADEND;
            conversationResponseNow.Nodes.Add(nodePy);
        }
        if(firstWord.Equals("response")) {
            string responseToTag = words[1];
            responseToTag = responseToTag.Replace(":", "");

            StartResponse();
            var talkNodes = conversationDataNow.TalkNodes;
            for (int i = 0; i < talkNodes.Count; i++) {
                var tagOfTalkNode = talkNodes[i].Tag;
                if(tagOfTalkNode == null) continue;
                if (tagOfTalkNode.Equals(responseToTag)) {
                    conversationResponseNow.Target = talkNodes[i];
                    break;
                }
            }
            

            return true;
        }
        if (firstWord.Equals("responsewrong")) {
            string slotString = words[1];
            int slot = (int) Char.GetNumericValue(slotString[4]);

            StartResponse();
            conversationResponseNow.Slot = slot;

            return true;
        }
        return false;
    }

    private void StartResponse() {
        Debug.Log("RESPONSE START!");
        parsingResponse = true;
        conversationResponseNow = new ConversationResponse();
        conversationDataNow.ConvResponses.Add(conversationResponseNow);
    }

    internal void CreateConversation() {
        parsingResponse = false;
        conversationDataNow = new ConversationData();

    }
}
