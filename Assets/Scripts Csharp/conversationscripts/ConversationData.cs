﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class ConversationData {
    [SerializeField]
    List<ConversationTalkNode> talkNodes = new List<ConversationTalkNode>();
    [SerializeField]
    List<ConversationResponse> convResponses = new List<ConversationResponse>();

    public List<ConversationTalkNode> TalkNodes {
        get {
            return talkNodes;
        }

        set {
            talkNodes = value;
        }
    }

    public List<ConversationResponse> ConvResponses {
        get {
            return convResponses;
        }

        set {
            convResponses = value;
        }
    }
}

[Serializable]
public class ConversationTalkNode {
    [SerializeField]
    NodePy node;
    [SerializeField]
    ConvTalkType talkType;
    [SerializeField]
    string tag;

    public string Tag {
        get {
            return tag;
        }

        set {
            tag = value;
        }
    }

    public ConvTalkType TalkType {
        get {
            return talkType;
        }

        set {
            talkType = value;
        }
    }

    public NodePy Node {
        get {
            return node;
        }

        set {
            node = value;
        }
    }
}

[Serializable]
public class ConversationResponse {
    ConversationTalkNode target;
    int slot = -1;
    [SerializeField]
    List<NodePy> nodes = new List<NodePy>();

    public List<NodePy> Nodes {
        get {
            return nodes;
        }

        set {
            nodes = value;
        }
    }

    public int Slot {
        get {
            return slot;
        }

        set {
            slot = value;
        }
    }

    public ConversationTalkNode Target {
        get {
            return target;
        }

        set {
            target = value;
        }
    }
}

[Serializable]
public enum ConvTalkType {
    FIXED,MOVABLE, DUMMY
}
