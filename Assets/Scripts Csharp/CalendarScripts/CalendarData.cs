﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CalendarData {
    Dictionary<int, string> dayToLabel = new Dictionary<int, string>();

    public Dictionary<int, string> DayToLabel {
        get {
            return dayToLabel;
        }

        set {
            dayToLabel = value;
        }
    }

    internal void AddLabel(int dayNumber, string label) {
        dayToLabel.Add(dayNumber, label);
    }
}
