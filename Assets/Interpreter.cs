﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using DG.Tweening;

public class Interpreter : MonoBehaviour {

    public DataHolderPy dataHolder;
    public TextManager textManager;
    int nodeIdExecuting;



    public ImageManager imageManager;
    public NodeEvent conversationStart;
    bool advancing = true;
    public UnityEvent AdvanceEvent;

    public UnityEvent CalendarAdvance;

    public StringEvent TextStart;
    public UnityEvent TextEnd;

    private bool waitingForInput;

    public bool Advancing {
        get {
            return advancing;
        }

        set {
            advancing = value;
        }
    }

    public string startLabel = "start";
    public StringEvent LogSendRequest;
    private int startNodeId;

    // Use this for initialization
    void OnEnable() {
        textManager.HideAll();
    }

    public void StartOnLabel(string label) {
        nodeIdExecuting = dataHolder.LabelPos(label);
        //dataHolder.DebugLabelData();
        Debug.Log(nodeIdExecuting + "LABEL!!!" + label + "end");
        if (nodeIdExecuting == -1) nodeIdExecuting = 0;
        startNodeId = nodeIdExecuting;
        Execute();
    }

    public void StartInterpreting() {

        StartOnLabel(startLabel);
    }

    public void Execute() {

        NodePy n = dataHolder.GetNode(nodeIdExecuting);
        Debug.Log("TRY EXECUTE" + n.Type);
        ExecuteNode(n);
    }

    public void ExecuteNode(NodePy n) {
        waitingForInput = false;

        if (n.Type == NodeType.LOGSEND) {
            LogSendRequest.Invoke(n.GetData(NodeDataEnum.LOG_TAG) as string);
            Advance();
        }

        if (n.Type == NodeType.SAY) {
            DoSayNode(n);

        } else {
            textManager.HideAll();
        }

        if (n.Type == NodeType.ENDGAME) {
            Application.Quit();

        }

        const float delayOnImage = 0.67f;
        if (n.Type == NodeType.SCENE) {
            bool gradual = true;
            DoSceneNode(n, gradual);
            Advance(delayOnImage);
        }

        if (n.Type == NodeType.CONTROLGROUPTIP) {
            
            Advance();
            return;
        }

        if (n.Type == NodeType.HIDE) {
            var imageToShow = n.GetData(NodeDataEnum.HIDE_IMAGE) as string;
            imageManager.HideImage(imageToShow);
            Advance(delayOnImage);
        }

        if (n.Type == NodeType.CALENDARADVANCEDAY) {
            CalendarAdvance.Invoke();
            return;
        }

        if (n.Type == NodeType.SHOW) {
            float xPos;
            string imageToShow;
            ShowProcessing(n, out xPos, out imageToShow);

            imageManager.ShowImage(imageToShow, xPos, 0.4f);
            Advance(delayOnImage);
        }
        if (n.Type == NodeType.CONVERSATIONASSEMBLE) {
            Debug.Log("CONVERSATION START!");
            conversationStart.Invoke(n);
            //return;
        }
    }

    private void DoSceneNode(NodePy n, bool gradual) {
        var imageToShow = n.GetData(NodeDataEnum.SHOW_IMAGE) as string;
        imageManager.HideAll(gradual);
        if (imageToShow != null)
            imageManager.ShowImage(imageToShow, 0.5f, 0.5f, gradual);
    }

    private void DoSayNode(NodePy n) {
        textManager.HideSome();
        waitingForInput = true;

        var textToShow = n.GetData(NodeDataEnum.SAY_TALK) as string;
        textManager.ResetAndShow(textToShow);
        TextStart.Invoke(textToShow);
        string speaker = n.GetData(NodeDataEnum.SAY_SPEAKER) as string;
        if (speaker != null) {

            var chara = dataHolder.Chara(speaker);
            var imageName = chara.ImageName;
            Debug.Log(imageName + " ...");
            if (imageName != null) {
                Debug.Log(imageName + " ...22");
                Image activeImage = imageManager.GetActiveImage(imageName);
                if (activeImage != null) {
                    Debug.Log(imageName + " ...33");
                    float x = activeImage.transform.localPosition.x;
                    if (x < -20) {
                        textManager.IndicatorShow(0);
                    } else {
                        if (x > 20) {
                            textManager.IndicatorShow(2);
                        } else {

                            textManager.IndicatorShow(1);

                        }
                    }
                }

            }
            var speakerName = chara.Name;
            if (speakerName != null && speakerName.Length > 0)
                textManager.ShowSpeaker(speakerName);
        } else {
            textManager.NoSpeaker();
        }
    }

    private void Advance(float v) {
        DOVirtual.DelayedCall(v, Advance);
    }

    public void AdvanceInputReceived() {
        if (textManager.isDone() && waitingForInput) {
            TextEnd.Invoke();
            Advance();
            //waitingForInput = false;
        } else {
            textManager.autoComplete();
        }
    }

    public void Advance() {
        Debug.Log("ADVANCE COMMAND!");
        AdvanceEvent.Invoke();
        if (advancing) {
            nodeIdExecuting++;
            Execute();
        }

    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(KeyCode.Backspace)) {
            RollbackInput();
        }


    }

    public void RollbackInput() {
        if (advancing && waitingForInput) {
            if (nodeIdExecuting > startNodeId) {
                var nodes = dataHolder.Nodes;
                bool rollbackSuccess = false;
                int previousIdExecution = nodeIdExecuting;
                for (int i = nodeIdExecuting - 1; i >= startNodeId; i--) {
                    var n = nodes[i];
                    if (n.Type == NodeType.CONVERSATIONASSEMBLE) {
                        break;
                    }
                    if (n.Type == NodeType.SAY) {
                        DoSayNode(n);
                        textManager.autoComplete();
                        textManager.autoCompleteEvenSpeakerChange();
                        nodeIdExecuting = i;
                        rollbackSuccess = true;
                        break;
                    }
                }
                if (rollbackSuccess) {
                    bool didScene = false;
                    int startId = 0;
                    for (int i = nodeIdExecuting - 1; i >= 0; i--) {
                        NodePy n = nodes[i];
                        if (n.Type == NodeType.SCENE) {
                            didScene = true;
                            DoSceneNode(n, false);
                            startId = i + 1;
                            break;
                        }
                    }
                    if (!didScene) {
                        imageManager.HideAll(false);
                    }
                    for (int i = startId; i < nodeIdExecuting; i++) {
                        NodePy n = nodes[i];
                        if (n.Type == NodeType.SHOW) {
                            DoShowInstant(n);
                        }
                        if (n.Type == NodeType.HIDE) {
                            imageManager.HideImage(
                                nodes[i].GetData(NodeDataEnum.HIDE_IMAGE) as string, false);
                        }
                    }
                }
                //nodeIdExecuting--;
                //Execute();
            }
        }
    }

    private void DoShowInstant(NodePy n) {
        float xPos;
        string imageToShow;
        ShowProcessing(n, out xPos, out imageToShow);

        imageManager.ShowImage(imageToShow, xPos, 0.4f, false);

    }

    private static void ShowProcessing(NodePy n, out float xPos, out string imageToShow) {
        var at = n.GetData(NodeDataEnum.SHOW_AT) as string;
        xPos = 0.5f;
        if (at != null) {
            if (at.Equals("right")) {
                xPos = 0.75f;
            }
            if (at.Equals("left")) {
                xPos = 0.25f;
            }
        }

        imageToShow = n.GetData(NodeDataEnum.SHOW_IMAGE) as string;
    }
}

[Serializable]
public class NodeEvent : UnityEvent<NodePy> {
}

[Serializable]
public class StringEvent : UnityEvent<string> { }