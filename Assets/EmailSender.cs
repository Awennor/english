﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.IO;
using System.Net.Mime;
using System.Collections.Generic;
using System.Threading;

public class SendingThread {
    internal string mailName;
    List<MailMessage> mails = new List<MailMessage>();
    List<MailDefiner> mailDefinitions = new List<MailDefiner>();
    int counter;
    internal bool running = true;

    public List<MailMessage> Mails {
        get {
            return mails;
        }

        set {
            mails = value;
        }
    }

    public int Counter {
        get {
            return counter;
        }

        set {
            counter = value;
        }
    }

    public List<MailDefiner> MailDefinitions {
        get {
            return mailDefinitions;
        }

        set {
            mailDefinitions = value;
        }
    }

    public void Beta() {
        while (running) {
            Counter++;
            //Debug.Log("Thread Running!");
            while (mails.Count > 0) {
                MailMessage mail = mails[0];
                mails.RemoveAt(0);
                SendMail(mail);

                //Debug.Log("success");
            }
            while (mailDefinitions.Count > 0) {
                MailDefiner mD = mailDefinitions[0];
                mailDefinitions.RemoveAt(0);
                using (MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(mD.attachBody))) {
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress(mailName);
                    mail.To.Add(mailName);
                    mail.Subject = mD.subject;
                    mail.Body = "";
                    Attachment attachment = new Attachment(stream, new ContentType(mD.contentType));
                    attachment.Name = mD.subject;
                    mail.Attachments.Add(attachment);

                    SendMail(mail);
                }
            }

        }
    }

    private void SendMail(MailMessage mail) {
        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
        smtpServer.Port = 587;
        smtpServer.Credentials = new System.Net.NetworkCredential(mailName, "senhapaia") as ICredentialsByHost;
        smtpServer.EnableSsl = true;
        ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
        try {
            smtpServer.Send(mail);
        } catch (Exception ex) {
            Debug.Log(" EXCEPTION CAUGHT " + ex.ToString());
            Console.WriteLine("Exception caught in Sending Mail Thread: {0}",
                        ex.ToString());
        }
    }
}

public class EmailSender : MonoBehaviour {

    string mailName = "datacollectionxone@gmail.com";
    SendingThread thread = new SendingThread();
    private Thread oThread;

    void Start() {


        string body = "This is for testing SMTP mail from GMAIL";
        string subject = "TestBody";
        oThread = new Thread(new ThreadStart(thread.Beta));
        thread.mailName = mailName;
        oThread.Start();
        //SendEmail(body, subject);

    }

    public void SendEmail(string subject, string body) {
        Debug.Log("" + subject + "XXX" + body);
        MailMessage mail = new MailMessage();
        mail.From = new MailAddress(mailName);
        mail.To.Add(mailName);
        mail.Subject = subject;
        mail.Body = body;
        SendMail(mail);
    }

    private void SendMail(MailMessage mail) {
        Debug.Log("THREAD COUNTER LOOP" + thread.Counter);

        thread.Mails.Add(mail);

    }

    public void SendMailWithTextPlain(string subject, string fileName, string fileBody) {
        MailDefiner mD = new MailDefiner();
        mD.subject = subject;
        mD.attachName = fileName;
        mD.attachBody = fileBody;
        mD.contentType = "text/plain";
        thread.MailDefinitions.Add(mD);
    }

    public void OnDestroy() {
        //oThread.Abort();
        thread.running = false;
    }

    public void SendMailWithCsv(string subject, string fileName, string fileBody) {
        MailDefiner mD = new MailDefiner();
        mD.subject = subject;
        mD.attachName = fileName;
        mD.attachBody = fileBody;
        mD.contentType = "text/csv";
        thread.MailDefinitions.Add(mD);
    }
}

public class MailDefiner {
    public string subject;
    public string body;
    public string attachBody;
    public string attachName;
    public string contentType;

}