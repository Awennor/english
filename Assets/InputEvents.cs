﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

public class InputEvents : MonoBehaviour {

    public InputUnit[] inputs;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < inputs.Length; i++) {
            if(Input.GetKeyDown(inputs[i].Keycode)) {
                inputs[i].KeyCallback.Invoke();
            }
            if(Input.GetButtonDown(inputs[i].ButtonName)) {
                inputs[i].KeyCallback.Invoke();
            }
        }
	}


}

[Serializable]
public class InputUnit {
    [SerializeField]
    KeyCode keycode;
    [SerializeField]
    UnityEvent keyCallback;
    [SerializeField]
    string buttonName;

    public KeyCode Keycode {
        get {
            return keycode;
        }

        set {
            keycode = value;
        }
    }

    public UnityEvent KeyCallback {
        get {
            return keyCallback;
        }

        set {
            keyCallback = value;
        }
    }

    public string ButtonName {
        get {
            return buttonName;
        }

        set {
            buttonName = value;
        }
    }
}