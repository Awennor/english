﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

class DOTweenCustomComponent : MonoBehaviour {

    public TweenUnit[] tweenUnits;

    public bool tweenOnEnable = true;


    public void OnEnable() {
        var seq = DOTween.Sequence();
        for (int i = 0; i < tweenUnits.Length; i++) {
            var tU = tweenUnits[i];
            var type = tU.tweenType;
            switch (type) {
                case DOTweenCompEnum.FadeIn:
                    seq.Append(GetComponent<Graphic>().DOFade(1, tU.time));
                    break;
                case DOTweenCompEnum.FadeOut:
                    seq.Append(GetComponent<Graphic>().DOFade(0, tU.time));
                    break;
                case DOTweenCompEnum.Callback:
                    if (tU.time > 0)
                        seq.AppendInterval(tU.time);
                    seq.AppendCallback(tU.uE.Invoke);
                    break;
                case DOTweenCompEnum.TimeScale:
                    if (tU.time > 0)
                        seq.AppendInterval(tU.time);
                    seq.AppendCallback(tU.EnforceTimescale);
                    break;
                case DOTweenCompEnum.Active:
                    if (tU.time > 0)
                        seq.AppendInterval(tU.time);
                    seq.AppendCallback(() => {
                        gameObject.SetActive(tU.valueB);
                    });
                    break;

                default:
                    break;
            }
        }
    }

    [Serializable]
    public class TweenUnit {

        public DOTweenCompEnum tweenType;
        public UnityEvent uE;
        public float time;
        public float value;
        public bool valueB;

        public void EnforceTimescale() {
            Time.timeScale = value;
        }

        internal void EnforceActive() {

        }
    }

}

public enum DOTweenCompEnum {
    FadeIn, Callback, TimeScale, Active, FadeOut,
}


