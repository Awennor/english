﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class TextManager : MonoBehaviour {

    public SlowText slowText;
    public Text charaName;
    public GameObject messageBox;
    public GameObject charaBox;
    public GameObject[] indicators;
    string currentSpeaker = null;
    private string textToShow;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void ResetAndShow(string v) {
        textToShow = v;
        ResetAndShow();
    }

    private void ResetAndShow() {
        //CancelSpeakerChangeAnimationOnce = false;
        slowText.ResetAndShow(textToShow);
        messageBox.transform.DOKill();
        messageBox.transform.DOScaleY(1f, 0.15f);
        //messageBox.SetActive(true);
    }

    internal bool isDone() {
        return slowText.isDone();
    }

    internal void autoComplete() {
        slowText.autoComplete();
    }

    internal void autoCompleteEvenSpeakerChange() {
        autoComplete();
        //CancelSpeakerChangeAnimationOnce = true;
        DOVirtual.DelayedCall(0.2f, autoComplete);
        messageBox.transform.DOKill();
    }

    internal void HideAll() {
        HideSome();
        //messageBox.SetActive(false);
        messageBox.transform.DOScaleY(0, 0.1f);
    }

    internal void HideSome() {
        for (int i = 0; i < indicators.Length; i++) {
            indicators[i].SetActive(false);
        }
        charaBox.SetActive(false);
    }

    internal void ShowMessageBox() {
        messageBox.SetActive(true);
    }

    internal void ShowSpeaker(string v) {
        if (currentSpeaker == null || !currentSpeaker.Equals(v)) {
            SpeakerChange();
        }

        currentSpeaker = v;
        UpdateTextColor();
        charaName.text = v;
        charaBox.SetActive(true);
    }

    private void UpdateTextColor() {
        Color c = new Color();
        Color.TryParseHexString("#61D0D0FF", out c);
        if (currentSpeaker != null)
            slowText.ChangeColor(new Color(1, 1, 1, 1f));
        else
            slowText.ChangeColor(c);
    }

    internal void IndicatorShow(int v) {
        indicators[v].SetActive(true);
    }

    internal void NoSpeaker() {
        
        if(currentSpeaker != null) {
            SpeakerChange();
        }
        currentSpeaker = null;
        UpdateTextColor();
        
    }

    private void SpeakerChange() {

                
        messageBox.transform.DOKill();
        messageBox.transform.DOScaleY(0, 0.1f);
        
        slowText.Cancel();
        DOVirtual.DelayedCall(0.15f, ResetAndShow);
    }

    
}
