﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;
using System.Collections.Generic;

[System.Serializable]
public class MyIntIntEvent : UnityEvent<int, int>{}
[System.Serializable]
public class MyIntStringEvent : UnityEvent<int, string>{}
[System.Serializable]
public class MyStringEvent : UnityEvent<string>{}
[System.Serializable]
public class MyBoolEvent : UnityEvent<bool>{}
[System.Serializable]
public class MyStringFloatEvent : UnityEvent<string, float>{}

public class DragAndDropManager : MonoBehaviour {

    public List<Draggable> draggables;
    public List<RectTransform> spots;
    public Canvas canvas;

	public UnityEvent AllSpotsFilled;
	public UnityEvent NotAllSpotsFilled;
    public MyIntIntEvent DragIdChangePosition;

    public float minDistance;
    public float breakDistance = -1;

    [SerializeField]
    IntVectorCustom draggableWhichSpot = new IntVectorCustom();

    public int DropSpaceAmount { get { return spots.Count; } }

    public IntVectorCustom SpotOfDraggable {
        get {
            return draggableWhichSpot;
        }

        set {
            draggableWhichSpot = value;
        }
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        float minDistance = this.minDistance * canvas.scaleFactor;
        for (int i = 0; i < draggables.Count; i++) {
			int oldSpotPos = SpotOfDraggable[i];
            var dragPos = draggables[i].transform.position;
            bool noLock = true;
            bool canBreak = true;
            for (int j = 0; j < spots.Count; j++) {
                var spotPos = spots[j].transform.position;
                float mag = (spotPos - dragPos).sqrMagnitude;
                if(breakDistance > 0 &&  oldSpotPos == j) {
                    if(mag < breakDistance*breakDistance) {
                        canBreak = false;
                    }
                }
                if (mag < minDistance * minDistance || (!canBreak &&  oldSpotPos == j)) {
                    noLock = false;
                    LockDraggableOnSpot(i,j);
                }
            }
            
            if (noLock && canBreak) {
                SpotOfDraggable[i] = -1;
                draggables[i].transform.rotation = Quaternion.identity;
            }
			if(oldSpotPos != SpotOfDraggable[i]){
                
                DragIdChangePosition.Invoke(i, SpotOfDraggable[i]);
                
				TryCallback();
			}
        }

    }

    internal void AddDraggable(Draggable draggable) {
        if(!draggables.Contains(draggable))
            draggables.Add(draggable);
    }

    internal void RemoveDraggable(Draggable draggable) {
        
        while(draggables.Remove(draggable));
    }

    internal void RemoveSlot(RectTransform gameObject) {
        while(spots.Remove(gameObject));
    }

    internal void AddSlot(RectTransform gameObject) {
        if(!spots.Contains(gameObject))
            spots.Add(gameObject);
    }

    internal bool IsCorrect(List<int> correctPositions) {
        for (int i = 0; i < correctPositions.Count; i++) {
            if(correctPositions[i] != FrameIndexOnSpot(i)) {
                return false;
            }
        }
        return true;
        
    }

    void TryCallback ()
	{
		bool allFill = true;
		for (int i = 0; i < spots.Count; i++) {
			if(FrameIndexOnSpot(i) == -1){
				allFill= false;
			}
		}
		if (allFill == true) {
			AllSpotsFilled.Invoke ();
		} else {
			NotAllSpotsFilled.Invoke ();

		}
	}

    internal int FrameIndexOnSpot(int spotIndex) {
        for (int i = 0; i < SpotOfDraggable.Length; i++) {
            if (SpotOfDraggable[i] == spotIndex) {
                return i;
            }
        }
        return -1;
    }

    internal void DebugSlotPositions() {
        
    }

    internal void LockDraggableOnSpot(int draggableId, int spotId) {
        int i = draggableId;
        int j = spotId;
        var spotPos = spots[j].transform.position;
        draggables[i].transform.position = spotPos;
        draggables[i].transform.rotation = spots[j].transform.rotation;
        SpotOfDraggable[i] = j;

    }
}
