﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
//using UnityEditor;

public class SlowText : MonoBehaviour {

    string text = "";
    string textSource;
    public float timePerChara = 0.08f;
    float timeDelta;
    public Text textField;
    public Action<char> oneCharShow;
    int offset;

    List<BoldData> boldDatas = new List<BoldData>();

    bool bolding = false;
    private string textSourceOriginal;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (textSource == null)
            return;
        if (!isDone()) {
            timeDelta -= Time.deltaTime;
            if (timeDelta < 0) {
                AdvanceTime();
                timeDelta = timePerChara;
            }

        }
    }

    private void AdvanceTime() {
        RefreshBolding();
        var currentIndex = GetCurrentIndex();
        var charAdded = textSource[currentIndex];

        if (bolding) {
            Debug.Log(text + "BEFORE BOLDINGSTART");
            text = text.Remove(text.Length - 4, 4);
            Debug.Log(text + "AFTER BOLDINGSTART");
        }
        text += charAdded;
        if (bolding) {
            text += "</b>";
        }
        if (oneCharShow != null) {

            oneCharShow(charAdded);
        }
        //EditorApplication.isPaused = true;
        
        UpdateTextField();
    }

    private int GetCurrentIndex() {
        return text.Length + offset;
    }

    private void RefreshBolding() {
        int index = GetCurrentIndex();
        for (int i = 0; i < boldDatas.Count; i++) {
            if(boldDatas[i].Start == index) {
                bolding = true;
                text+= "<b></b>";
                offset -= 7;
            }
            if(boldDatas[i].End == index) {
                bolding = false;
            }
            //if(boldDatas[i].Start < index && boldDatas[i].End ) {
            //}
        }
    }

    private void UpdateTextField()
    {
        if (textField != null)
            textField.text = text;
    }

    public bool isDone() {
        if(textSource == null) return false;
        return text.Length+offset == textSource.Length;

    }

    internal void ResetAndShow(string s)
    {
        bolding = false;
        offset = 0;
        text = "";
        textSourceOriginal = s;
        timeDelta = timePerChara;
        
        for (int i = 0; i < boldDatas.Count; i++) {
            boldDatas[i].Start = -1;
            boldDatas[i].End = -1;
        }
        while(s.Contains("<b>") && s.Contains("</b>")) {
            int start = s.IndexOf("<b>");
            s = s.Remove(start, 3);
            int end = s.IndexOf("</b>");
            s = s.Remove(end, 4);
            BoldData b = GetUnusedBoldData();
            b.Start = start;
            b.End = end;
        }
        Debug.Log(s + "TEXT BEING SHOWN");
        textSource = s;
        textField.text = "";
    }

    private BoldData GetUnusedBoldData() {
        for (int i = 0; i < boldDatas.Count; i++) {
            if(boldDatas[i].Start == -1) {
                return boldDatas[i];
            }
            
        }
        BoldData b = new BoldData();
        boldDatas.Add(b);
        return b;
    }

    internal void Cancel()
    {
        text = "";
        timeDelta = timePerChara;
        textSource = "";
        textField.text = "";
    }

    internal void autoComplete()
    {
        offset = 0;
        bolding = false;
        text = textSourceOriginal;
        textSource = textSourceOriginal;
        UpdateTextField();
        //text = textSource;
        //UpdateTextField();
    }

    internal void ChangeColor(Color color) {
        textField.color = color;
    }
}

class BoldData {
    int start = -1;
    int end = -1;

    public int Start {
        get {
            return start;
        }

        set {
            start = value;
        }
    }

    public int End {
        get {
            return end;
        }

        set {
            end = value;
        }
    }
}